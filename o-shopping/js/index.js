var app = angular.module('WebApp', ['ngMaterial','ngAnimate','firebase']);
'use strict';

// controller
app.controller('AppCtrl', ['$scope', 
'$mdSidenav','$firebaseArray', function($scope, $firebaseArray, $mdSidenav){
  // connect to firebase
  var ref = new Firebase("https://webfinal-31b31.firebaseio.com/");
  
  $scope.NameShop="O-Shopping";
  $scope.Home="Trang chủ";
  $scope.Introduction="Giới thiệu";
  $scope.Contact="Liên hệ";
  $scope.ListProducts="Danh mục sản phẩm";
  
  // Introduce our website
  $scope.Introduce="Chúng tôi là những người tiên phong trong việc phân phối dịch vụ thức ăn nhanh, tiếp thu toàn bộ công nghệ trong việc quản lý. Chúng tôi hướng đến tạo chuỗi thương hiệu thức ăn nhanh phát triển theo quy trình chuyên nghiệp, kinh doanh thức ăn phù hợp với tầng lớp trung lưu ngày càng gia tăng."
  // Display contact information
  $scope.InforContact=[
    {
      Name:"Phạm Hoàng Huy",
      Phone:"01284884900",
      Email:"phhuy2450@gmail.com"
    }
  ];
  // option for orderBy
  $scope.Kinds = [
    {
      nameKind:"Tên tăng dần",
      OrderByKind:"name"},
    {
      nameKind:"Tên giảm dần",
      OrderByKind:"-name"},
    {
      nameKind:"Giá tăng dần",
      OrderByKind:"cost"},
    {
      nameKind:"Giá giảm dần",
      OrderByKind:"-cost"}
    ];

  // declare empty array
  $scope.List1s = [];
  $scope.List2s = [];
  $scope.List3s = [];
  $scope.List4s = [];
  
  ref.on('child_added', function(snap) {
    var newStorage = snap.val();
    newStorage.cost = parseFloat(newStorage.cost);
    /* kind:
    =1: Nước uống
    =2: Bánh
    =3: Cơm văn phòng
    =4: Các sản phẩm khác
    */
    switch(newStorage.kind){
      case 1: $scope.List1s.push(newStorage);
        break;
      case 2: $scope.List2s.push(newStorage);
        break;
      case 3: $scope.List3s.push(newStorage);
        break;
      default: 
        $scope.List4s.push(newStorage);
    }
  });
  
  // show Introduce first
  $scope.introduce=true;
  // TAB Introduce is pressed
  $scope.showIntroduce=function(){
    $scope.DisplayContact=false;
    $scope.introduce=true;
    $scope.content1=false;
    $scope.content2=false;
    $scope.content3=false;
    $scope.content4=false;
  };
  
  // TAB Contact is pressed
  $scope.showContact=function(){
    $scope.DisplayContact=true;
    $scope.introduce=false;
    $scope.content1=false;
    $scope.content2=false;
    $scope.content3=false;
    $scope.content4=false;
  };
  
  // button 1 is pressed
  $scope.myButton1=function(){
    $scope.DisplayContact=false;
    $scope.introduce=false;
    $scope.content1=true;
    $scope.content2=false;
    $scope.content3=false;
    $scope.content4=false;
  };
  
  // button 2 is pressed
  $scope.myButton2=function(){
    $scope.DisplayContact=false;
    $scope.introduce=false;
    $scope.content2=true;
    $scope.content1=false;
    $scope.content3=false;
    $scope.content4=false;
  };
  
  // button 3 is pressed
  $scope.myButton3=function(){
    $scope.DisplayContact=false;
    $scope.introduce=false;
    $scope.content3=true;
    $scope.content1=false;
    $scope.content2=false;
    $scope.content4=false;
  };
  
  // button 4 is pressed
  $scope.myButton4=function(){
    $scope.DisplayContact=false;
    $scope.introduce=false;
    $scope.content4=true;
    $scope.content1=false;
    $scope.content2=false;
    $scope.content3=false;
  };
 
  
  $scope.toggleSidenav = function(menuId) {
    $mdSidenav(menuId).toggle();
  };
}]);