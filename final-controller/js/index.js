(function() {
  'use strict';
  var app = angular.module('WebApp', ['ui.router', "firebase"]);
  
  app.run(function($rootScope, $location, $state, LoginService) {
    $rootScope.$on('$stateChangeStart', 
      function(event, toState, toParams, fromState, fromParams){ 
          console.log('Changed state to: ' + toState);
      });
    
      if(!LoginService.isAuthenticated()) {
        $state.transitionTo('login');
      }
  });
  
  app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/product');
    
    $stateProvider
      .state('login', {
        url : '/login',
        templateUrl : 'login.html',
        controller : 'LoginController'
      })
      .state('product', {
        url : '/product',
        templateUrl : 'product.html',
        controller : 'ProductController'
      });
  }]);

  // Main controller
  app.controller('LoginController', function($scope, $rootScope, $stateParams, $state, LoginService) {
    $rootScope.title = "TRANG QUẢN LÝ SẢN PHẨM";
    
    $scope.formSubmit = function() {
      if(LoginService.login($scope.username, $scope.password)) {
        $scope.error = '';
        $scope.username = '';
        $scope.password = '';
        $state.transitionTo('product');
      } else {
        $scope.error = "Incorrect username/password !";
      }   
    };
    
  });
  
  // Product Controller
app.controller("ProductController", function($scope, $firebaseArray) {
   var ref = new Firebase("https://webfinal-31b31.firebaseio.com/");
   
   $scope.products = $firebaseArray(ref);
  
   $scope.addProduct = function() {
      $scope.products.$add({
         kind: $scope.kind,
         name: $scope.name,
         image: $scope.image,
         cost: $scope.cost,
         content: $scope.content
      }).then(function(ref){
        var id = ref.key();
        console.log('Added product ' + id);
      $scope.kind = "";
      $scope.name = "";
      $scope.image = "";
      $scope.cost = "";
      $scope.content = "";});
   }
   
   $scope.AddFormShow = true;
   $scope.EditFormShow = false;
  
   $scope.showEditForm = function(product){
     $scope.AddFormShow = false;
     $scope.EditFormShow = true;
     
     $scope.id = product.$id;
     $scope.kind = product.kind;
     $scope.name = product.name;
     $scope.image = product.image;
     $scope.cost = product.cost;
     $scope.content = product.content;
   }
   
   $scope.editProduct = function(){
     var id = $scope.id;
     
     var record = $scope.products.$getRecord(id);
     
     record.kind = $scope.kind;
     record.name = $scope.name;
     record.image = $scope.image;
     record.cost = $scope.cost;
     record.content = $scope.content;
     
     // Save information
     $scope.products.$save(record).then(
       function(ref){
         console.log(ref.key);
     });
     
     $scope.kind = "";
     $scope.name = "";
     $scope.image = "";
     $scope.cost = "";
     $scope.content = "";
   }
   
   
   
   });
  
  app.factory('LoginService', function() {
    var admin = 'admin';
    var pass = 'pass';
    var isAuthenticated = false;
    
    return {
      login : function(username, password) {
        isAuthenticated = username === admin && password === pass;
        return isAuthenticated;
      },
      isAuthenticated : function() {
        return isAuthenticated;
      }
    };
    
  });
  
})();